/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.oovoo.sdk.sample.plugin;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.oovoo.sdk.sample.plugin";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
