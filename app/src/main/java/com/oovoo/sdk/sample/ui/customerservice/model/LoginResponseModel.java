package com.oovoo.sdk.sample.ui.customerservice.model;

/**
 * Created by yarlagadda on 21-12-2016.
 */

public class LoginResponseModel {
    int PassCodeID;
    int TransfereeID;
    String Salt;
    String UserName;
    String Password;
    int ApplicationSource;
    String TranfereeFirstName;
    String TranfereeLastName;
    String Address;
    String City;
    String State;
    String ZipCode;
    String EmailAddress;
    int ClientID;

    public int getPassCodeID() {
        return PassCodeID;
    }

    public void setPassCodeID(int passCodeID) {
        PassCodeID = passCodeID;
    }

    public int getTransfereeID() {
        return TransfereeID;
    }

    public void setTransfereeID(int transfereeID) {
        TransfereeID = transfereeID;
    }

    public String getSalt() {
        return Salt;
    }

    public void setSalt(String salt) {
        Salt = salt;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public int getApplicationSource() {
        return ApplicationSource;
    }

    public void setApplicationSource(int applicationSource) {
        ApplicationSource = applicationSource;
    }

    public String getTranfereeFirstName() {
        return TranfereeFirstName;
    }

    public void setTranfereeFirstName(String tranfereeFirstName) {
        TranfereeFirstName = tranfereeFirstName;
    }

    public String getTranfereeLastName() {
        return TranfereeLastName;
    }

    public void setTranfereeLastName(String tranfereeLastName) {
        TranfereeLastName = tranfereeLastName;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }

    public int getClientID() {
        return ClientID;
    }

    public void setClientID(int clientID) {
        ClientID = clientID;
    }
}
