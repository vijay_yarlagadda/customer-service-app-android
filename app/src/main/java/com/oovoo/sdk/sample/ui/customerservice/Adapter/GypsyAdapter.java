package com.oovoo.sdk.sample.ui.customerservice.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.allmysons.customeserviceapp.R;
import com.allmysons.customeserviceapp.model.GypsyModel;
import com.allmysons.customeserviceapp.ui.Gypsy;
import com.allmysons.customeserviceapp.ui.GypsyMothIndividual;

import java.util.List;

import static com.allmysons.customeserviceapp.R.id.imageView;

/**
 * Created by Gajjala's on 11/2/2016.
 */

public class GypsyAdapter extends BaseAdapter implements View.OnClickListener {
    List<GypsyModel> gypsyModels;
    Context context;
    private static LayoutInflater inflater=null;

    public GypsyAdapter(List<GypsyModel> gypsyModels, Context context) {
        this.gypsyModels = gypsyModels;
        this.context = context;
        inflater=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return gypsyModels.size();
    }

    @Override
    public Object getItem(int i) {
        return gypsyModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, final View view, ViewGroup viewGroup) {
        ViewHolder   holder = new ViewHolder();
        View vi=inflater.inflate(R.layout.gypsymoth_checklist_items,null);

        holder.textView=(TextView)vi.findViewById(R.id.issue_tv);
        holder.textView1=(TextView)vi.findViewById(R.id.backpacks_tv);
        holder.imageView=(ImageView)vi.findViewById(R.id.issue_IMV);
        holder.backpacks_LI=(LinearLayout)vi.findViewById(R.id.backpacks_LI);

        holder.textView.setText(gypsyModels.get(i).backboards);
        holder.textView1.setText(gypsyModels.get(i).issue);
        holder.imageView.setImageResource(gypsyModels.get(i).cross);
        holder.imageView.setTag(i);
        holder.backpacks_LI.setOnClickListener(this);
        final ViewHolder finalHolder = holder;
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int position = (Integer) v.getTag();
                finalHolder.imageView.setImageResource(gypsyModels.get(position).tickmark);
                finalHolder.textView.setText(gypsyModels.get(position).goodtext);
            }
        });
        return vi;
    }
    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.backpacks_LI){
            Intent selfinspection=new Intent(context,GypsyMothIndividual.class);
            context.startActivity(selfinspection);
        }

    }
    class ViewHolder{
        ImageView imageView;
        TextView textView,textView1;
        LinearLayout backpacks_LI;

    }
}
