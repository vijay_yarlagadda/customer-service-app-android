package com.oovoo.sdk.sample.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MenuItem;

import com.oovoo.sdk.api.LogSdk;
import com.oovoo.sdk.api.ooVooClient;
import com.oovoo.sdk.sample.R;
import com.oovoo.sdk.sample.app.ooVooSdkSampleShowApp;
import com.oovoo.sdk.sample.services.RegistrationIntentService;
import com.oovoo.sdk.sample.ui.SampleActivity;
import com.oovoo.sdk.sample.ui.fragments.BaseFragment;
import com.oovoo.sdk.sample.ui.fragments.CallIntro;
import com.oovoo.sdk.sample.ui.fragments.CallList;
import com.oovoo.sdk.sample.ui.fragments.CallMake;
import com.oovoo.sdk.sample.ui.fragments.CallOn;
import com.oovoo.sdk.sample.ui.fragments.PushNotificationFragment;
import com.oovoo.sdk.sample.ui.fragments.ReautorizeFragment;
import com.oovoo.sdk.sample.ui.fragments.SplashScreen;
import com.oovoo.sdk.sample.ui.fragments.WaitingFragment;

/**
 * Created by yarlagadda on 01-12-2016.
 */

public class CallScreenUIActivity extends Activity implements ooVooSdkSampleShowApp.OperationChangeListener {
    private ooVooSdkSampleShowApp	application	   = null;
    private static final String	  	TAG	           	= SampleActivity.class.getSimpleName();
    protected BaseFragment	      	current_fragment	= null;
    private static final String 	STATE_FRAGMENT 	= "current_fragment";
    private boolean					mIsAlive = false;
    private boolean					mNeedShowFragment = false;
    private MenuItem mSettingsMenuItem = null;
    private AlertDialog callDialogBuilder = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (ooVooSdkSampleShowApp) getApplication();
        application.setContext(this);

        setRequestedOrientation(application.getDeviceDefaultOrientation());

        setContentView(R.layout.host_activity);

        application.addOperationChangeListener(this);

        if (savedInstanceState != null) {
            current_fragment = (BaseFragment)getFragmentManager().getFragment(savedInstanceState, STATE_FRAGMENT);
            showFragment(current_fragment);
        } else {
            Fragment newFragment = new SplashScreen();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(R.id.host_activity, newFragment).commit();

            if (!ooVooClient.isDeviceSupported()) {
                return;
            }

            try {
                application.onMainActivityCreated();
            } catch( Exception e) {
                Log.e( TAG, "onCreate exception: ", e);
            }
        }

    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        try {
            getFragmentManager().putFragment(savedInstanceState, STATE_FRAGMENT, current_fragment);
        } catch( Exception e) {
            Log.e( TAG, "onSaveInstanceState exception: ", e);
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        application.removeOperationChangeListener(this);
    }
    @Override
    public void onOperationChange(ooVooSdkSampleShowApp.Operation state) {
        try {
            switch (state) {
                case Error:
                {
                    switch (state.forOperation()) {
                        case Authorized:
                            current_fragment = ReautorizeFragment.newInstance(mSettingsMenuItem, state.getDescription());
                            break;
                        case LoggedIn:
                            current_fragment = CallIntro.newInstance(state.getDescription());
                            break;
                        case AVChatJoined:
                            //application.showErrorMessageBox(this, getString(R.string.join_session), state.getDescription());
                            //current_fragment = AVChatLoginFragment.newInstance(mSettingsMenuItem);
                            break;
                        default:
                            return;
                    }
                }
                break;
                case Processing:
                    current_fragment = WaitingFragment.newInstance(state.getDescription());
                    break;
                case AVChatRoom:
                    //current_fragment = AVChatLoginFragment.newInstance(mSettingsMenuItem);
                    break;
                case AVChatCall:
                    current_fragment = CallList.newInstance(mSettingsMenuItem);
                    break;
                case PushNotification:
                    current_fragment = PushNotificationFragment.newInstance(mSettingsMenuItem);
                    break;
                case AVChatJoined:
                    //current_fragment = CallOn.newInstance(mSignalStrengthMenuItem,mSecureNetworkMenuItem, mInformationMenuItem);
                    break;
                case Authorized:
                    current_fragment = CallIntro.newInstance(mSettingsMenuItem);
                    break;
                case LoggedIn:
                   /* if (checkPlayServices()) {
                        // Start IntentService to register this application with GCM.
                        Intent intent = new Intent(this, RegistrationIntentService.class);
                        startService(intent);
                    }*/
                    current_fragment = CallMake.newInstance(mSettingsMenuItem);
                    break;
                case AVChatDisconnected:

                    if (application.isCallNegotiation()) {
                        return;
                    } else {
                        //current_fragment = AVChatLoginFragment.newInstance(mSettingsMenuItem);
                        break;
                    }

                default:
                    return;
            }

            showFragment(current_fragment);

        } catch (Exception err) {
            err.printStackTrace();
        }
    }
    protected void showFragment(Fragment newFragment) {
        if(!mIsAlive){
            mNeedShowFragment = true;
            return;
        }

        try {
            if (newFragment != null) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.host_activity, newFragment);
                transaction.addToBackStack(newFragment.getClass().getSimpleName());
                transaction.commit();
            }
        }
        catch(Exception err){
            LogSdk.e(TAG,"showFragment " + err);
        }
    }
    private void removeFragment(Fragment fragment) {

        try {
            if (fragment != null) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.remove(current_fragment);
                transaction.show(fragment);
                transaction.commit();
            }
        }
        catch(Exception err){
            LogSdk.e(TAG,"removeFragment " + err);
        }
    }
}
