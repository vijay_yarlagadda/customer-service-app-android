package com.oovoo.sdk.sample.ui.fragments;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oovoo.sdk.api.LogSdk;
import com.oovoo.sdk.api.Message;
import com.oovoo.sdk.api.sdk_error;
import com.oovoo.sdk.interfaces.AudioRoute;
import com.oovoo.sdk.interfaces.AudioRouteController;
import com.oovoo.sdk.interfaces.Device;
import com.oovoo.sdk.interfaces.Effect;
import com.oovoo.sdk.interfaces.VideoController;
import com.oovoo.sdk.interfaces.VideoControllerListener;
import com.oovoo.sdk.interfaces.ooVooSdkResult;
import com.oovoo.sdk.interfaces.ooVooSdkResultListener;
import com.oovoo.sdk.sample.R;
import com.oovoo.sdk.sample.app.ApplicationSettings;
import com.oovoo.sdk.sample.app.ooVooSdkSampleShowApp;
import com.oovoo.sdk.sample.ui.SampleActivity;
import com.oovoo.sdk.sample.ui.SignalBar;
import com.oovoo.sdk.sample.ui.animations.ZeroGravityAnimation;
import com.oovoo.sdk.sample.ui.animations.direction.Direction;
import com.oovoo.sdk.sample.ui.controllers.CustomMYView;
import com.oovoo.sdk.sample.ui.customerservice.ui.ConfirmInfo;
import com.oovoo.sdk.sample.ui.fragments.BaseFragment;
import com.oovoo.sdk.sample.ui.fragments.CallOn;

import java.util.ArrayList;

/**
 * Created by yarlagadda on 02-12-2016.
 */

public class InCallScreenFragment extends BaseFragment implements ooVooSdkSampleShowApp.ParticipantsListener,ooVooSdkSampleShowApp.MessageController,/*MessagingListener,*/ ooVooSdkSampleShowApp.CallControllerListener, View.OnClickListener, ooVooSdkSampleShowApp.NetworkListener {
    String[] percentage = {"10%","20%","30%","40%"};
    public boolean textChange = false;
    @Override
    public void updateMSGController(Message message) {
        if(!textChange) {
            conferenceViewController.setFullrHalfDisplay(false);
            linearLayout.setVisibility(View.VISIBLE);
            linearLayout.startAnimation(popupAnimation);
            popTextView.setText("Offer on your vehicle");
            if (message.getBody() != null) {
                int val = Integer.parseInt(message.getBody());
                offerPercent.setText(percentage[val]);
            }
            textChange = true;
        }else{
            popTextView.setText("Offer on your vehicle");
            if (message.getBody() != null) {
                int val = Integer.parseInt(message.getBody());
                offerPercent.setText(percentage[val]);
            }

        }
    }

    public enum CameraState {
        BACK_CAMERA(0), FRONT_CAMERA(1), MUTE_CAMERA(2);

        private final int value;

        private CameraState(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    protected static final String TAG = CallOn.class.getSimpleName();
    private View self = null;
    private Button microphoneBttn = null;
    private Button speakerBttn = null;
    private Button cameraBttn = null;
    private ImageView endOfCall = null;
    private View callbar = null;
    private MenuItem signalStrengthMenuItem = null;
    private MenuItem secureNetworkMenuItem = null;
    private MenuItem informationMenuItem = null;
    private CallOn.CameraState cameraState = CallOn.CameraState.FRONT_CAMERA;
    private CustomMYView conferenceViewController = null;
    private boolean isLayoutFinished = false;
    private ArrayList<Effect> videoFilters = null;
    TextView offerPercent;

    public CallOn() {
    }

    public static final CallOn newInstance(MenuItem signalStrengthMenuItem,
                                           MenuItem secureNetworkMenuItem,
                                           MenuItem informationMenuItem) {
        CallOn instance = new CallOn();
        instance.setSignalStrengthMenuItem(signalStrengthMenuItem);
        instance.setSecureNetworkMenuItem(secureNetworkMenuItem);
        instance.setInformationMenuItem(informationMenuItem);

        return instance;
    }

    public void setSignalStrengthMenuItem(MenuItem signalStrengthMenuItem) {
        this.signalStrengthMenuItem = signalStrengthMenuItem;
    }

    public void setSecureNetworkMenuItem(MenuItem secureNetworkMenuItem) {
        this.secureNetworkMenuItem = secureNetworkMenuItem;
    }

    public void setInformationMenuItem(MenuItem informationMenuItem) {
        this.informationMenuItem = informationMenuItem;
    }
    ImageView image1, image2,image3,image4;
    Animation animationSlideDownIn, animationSlideDownOut,popupAnimation;
    ImageView curSlidingImage;
    LinearLayout linearLayout;
    TextView popTextView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        self = inflater.inflate(R.layout.call_on, container, false);
        //filters = app().getVideoFilters();
        videoFilters = app().getVideoFilters();
        initControlBar(self);
        container = (ViewGroup) self.findViewById(R.id.popupDialog);
        conferenceViewController = new CustomMYView(self, getActivity(), app());
        conferenceViewController.getConferenceView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (!isLayoutFinished) {
                    addParticipantVideoPanel(ApplicationSettings.PREVIEW_ID, "Me");
                    //addParticipantVideoPanel("999999","test");
                    isLayoutFinished = true;
                }
            }
        });

        app().addParticipantListener(this);
        app().setControllerListener(this);
        //app().setMessageLister(this);
        app().setMessageControllerListener(this);

        try {
            String securityState = settings().get(ApplicationSettings.SecurityState);
            if (securityState != null && Boolean.valueOf(securityState)) {
                secureNetworkMenuItem.setIcon(getResources().getDrawable(R.drawable.menu_ic_lock));
            } else {
                secureNetworkMenuItem.setIcon(getResources().getDrawable(R.drawable.menu_ic_lock_unlock));
            }
        }
        catch(Exception err){
            LogSdk.e(TAG,"onCreateView "+err);
        }
        image1 = (ImageView)self.findViewById(R.id.imageView);
        image2 = (ImageView)self.findViewById(R.id.imageView2);
        //image3 = (ImageView)self.findViewById(R.id.test1);
        //image4 = (ImageView)self.findViewById(R.id.test2);
        linearLayout = (LinearLayout)self.findViewById(R.id.popupDialog);
        offerPercent = (TextView)self.findViewById(R.id.percentID);
        popTextView = (TextView)self.findViewById(R.id.popText);
        final Animation animAccelerateDecelerate = AnimationUtils.loadAnimation(getActivity(), R.anim.anticipate_overshoot);
        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessageing("999999","Like");
                flyStars(R.drawable.smiley);
            }
        });
        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        popupAnimation = AnimationUtils.loadAnimation(getActivity(),R.anim.diagonal_animation);
        popupAnimation.setAnimationListener(popupListerner);
        return self;
    }


    public void sendMessageing(String to_userID,String msg) {
        try {
            Message message = new Message(to_userID, msg);
            app().messageSend(message);
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        }
    }

    Animation.AnimationListener popupListerner
            = new Animation.AnimationListener(){

        @Override
        public void onAnimationEnd(Animation arg0) {
            // TODO Auto-generated method stub
            linearLayout.setVisibility(View.INVISIBLE);
            conferenceViewController.setFullrHalfDisplay(true);
            textChange = false;

        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onAnimationStart(Animation animation) {
            // TODO Auto-generated method stub

        }

    };

    private void initControlBar(View callbar) {
        this.callbar = callbar;

        endOfCall = (ImageView) callbar.findViewById(R.id.phone_green);
        endOfCall.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ConfirmInfo.class));
                getActivity().overridePendingTransition(R.anim.enter_activity, R.anim.exit_activity);
                endOfCall.setEnabled(false);
                app().endCall(new ooVooSdkResultListener() {
                    @Override
                    public void onResult(ooVooSdkResult ooVooSdkResult) {
                        app().sendEndCall();
                        endOfCall.setEnabled(true);
                        int count = getFragmentManager().getBackStackEntryCount();
                        String name = getFragmentManager().getBackStackEntryAt(
                                - 2).getName();
                        getFragmentManager().popBackStack(name, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                });
            }
        });

        // app().selectCamera("FRONT");
        app().changeResolution(VideoController.ResolutionLevel.ResolutionLevelMed);
        app().openPreview();
//        app().startTransmit();
        settings().put(ApplicationSettings.ResolutionLevel, toResolutionString(VideoController.ResolutionLevel.ResolutionLevelMed));

        prepareButtonMenu((Button) callbar.findViewById(R.id.button), new SampleActivity.MenuList() {
            @Override
            public void fill(View view, ContextMenu menu) {
                try {
                    menu.setHeaderTitle(R.string.resolution);
                    menu.setGroupCheckable(view.getId(), true, true);
                    String activeResolution = toResolutionString(app().getActiveResolution());

                    for (VideoController.ResolutionLevel resolution : app().getAvailableResolutions()) {
                        MenuItem item = menu.add(toResolutionString(resolution));
                        item.setOnMenuItemClickListener(new CallOn.ResolutionMenuClickListener(resolution) {

                            @Override
                            public boolean onMenuItemClick(VideoController.ResolutionLevel resolution, MenuItem item) {
                                app().changeResolution(resolution);
                                settings().put(ApplicationSettings.ResolutionLevel, item.getTitle().toString());
                                item.setChecked(true);
                                return false;
                            }
                        });

                        if (item.getTitle().toString().equals(activeResolution)) {
                            item.setChecked(true);
                        }

                        item.setCheckable(true);
                    }

                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });

        app().getAudioRouteController().setListener(new AudioRouteController.AudioRouteControllerListener() {
            @Override
            public void onAudioRouteChanged(AudioRoute audioRoute, AudioRoute audioRoute1) {
                onAudioRouteChangedEvent(audioRoute, audioRoute1);
            }
        });

        app().setMicMuted(false);
        app().setSpeakerMuted(false);

        ArrayList<AudioRoute> routes = app().getAudioRouteController().getRoutes();
        for (AudioRoute route : routes) {
            if (route.isActive()){}
            // updateRouteButtonImage(route);
        }

        //updateController();
    }

    private String toResolutionString(VideoController.ResolutionLevel level) {
        String friendlyName = "";
        switch (level) {
            case ResolutionLevelLow:
                friendlyName = "Low";
                break;
            case ResolutionLevelMed:
                friendlyName = "Medium";
                break;
            case ResolutionLevelHigh:
                friendlyName = "High";
                break;
            case ResolutionLevelHD:
                friendlyName = "HD";
                break;
            default:
                break;
        }
        return friendlyName;
    }

    @Override
    public void onResume() {

        try {
            app().setNetworkListener(this);
            signalStrengthMenuItem.setVisible(true);
            secureNetworkMenuItem.setVisible(true);
            informationMenuItem.setVisible(true);

        } catch (Exception err) {
            LogSdk.e(TAG, "onResume" + err);
        }

        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

        try {
            app().setNetworkListener(null);
            signalStrengthMenuItem.setVisible(false);
            secureNetworkMenuItem.setVisible(false);
            informationMenuItem.setVisible(false);
        } catch (Exception err) {

        }
    }

    @Override
    public void onStop() {

        super.onStop();
    }

    @Override
    public void onDestroy() {
        try {
            app().removeParticipantListener(this);
            app().setControllerListener(null);
            super.onDestroy();
        } catch (Exception err) {

        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (app().isTablet()) {
            conferenceViewController.onConfigurationChanged();
        }
    }

    public void onTransmitStateChanged(boolean state, sdk_error error) {

    }

    @Override
    public void onRemoteVideoStateChanged(final String userId, final VideoControllerListener.RemoteVideoState state, final sdk_error error) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (state) {
                    case RVS_Started:
                    case RVS_Resumed:
                        conferenceViewController.hideNoVideoMessage(userId);
                        break;
                    case RVS_Stopped:
                        break;
                    case RVS_Paused:
                        conferenceViewController.showNoVideoMessage(userId);
                        break;
                }

                if (error == sdk_error.ResolutionNotSupported) {
                    conferenceViewController.showAvatar(userId);
                }
            }
        });
    }

    @Override
    public void onParticipantJoined(final String userId, final String userData) {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    addParticipantVideoPanel(userId, userData);
                }
            });
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    protected void addParticipantVideoPanel(String userId, String userData) {
        try {
            conferenceViewController.addParticipant(userId, userData);
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    @Override
    public void onParticipantLeft(final String userId) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                conferenceViewController.removeParticipant(userId);
            }
        });
    }

    @Override
    public void updateController() {
        try {
            microphoneBttn.setEnabled(true);
            speakerBttn.setEnabled(true);
            microphoneBttn.setSelected(app().isMicMuted());
            speakerBttn.setSelected(app().isSpeakerMuted());
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (v instanceof Button && v.getTag() instanceof SampleActivity.MenuList) {
            v.showContextMenu();
        }
    }

    private void prepareButtonMenu(final Button button, SampleActivity.MenuList list) {
        button.setOnClickListener(this);
        button.setTag(list);
        getActivity().registerForContextMenu(button);
    }

    protected void onAudioRouteChangedEvent(AudioRoute old_route, AudioRoute new_route) {
        updateRouteButtonImage(new_route);
    }

    /**
     * When audio route changes we change button image too.
     *
     * @param new_route
     */
    private void updateRouteButtonImage(AudioRoute new_route) {
        try {
            Button button = (Button) callbar.findViewById(R.id.audioRoutes);
            switch (new_route.getRouteId()) {
                case AudioRoute.Earpiece:
                    button.setBackgroundResource(R.drawable.earpiece_selector);
                    break;
                case AudioRoute.Speaker:
                    button.setBackgroundResource(R.drawable.speakers_selector);
                    break;
                case AudioRoute.Headphone:
                    button.setBackgroundResource(R.drawable.headphone_selector);
                    break;
                case AudioRoute.Bluetooth:
                    button.setBackgroundResource(R.drawable.bluetooth_selector);
                    break;
            }
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    abstract class DeviceMenuClickListener implements MenuItem.OnMenuItemClickListener {
        private Device device = null;

        DeviceMenuClickListener(Device device) {
            this.device = device;
        }

        @Override
        public final boolean onMenuItemClick(MenuItem item) {
            return onMenuItemClick(device, item);
        }

        public abstract boolean onMenuItemClick(Device device, MenuItem item);
    }

    abstract class AudioRouteMenuClickListener implements MenuItem.OnMenuItemClickListener {
        private AudioRoute route = null;

        AudioRouteMenuClickListener(AudioRoute route) {
            this.route = route;
        }

        @Override
        public final boolean onMenuItemClick(MenuItem item) {
            return onMenuItemClick(route, item);
        }

        public abstract boolean onMenuItemClick(AudioRoute route, MenuItem item);
    }

    abstract class EffectMenuClickListener implements MenuItem.OnMenuItemClickListener {
        private Effect effect = null;

        EffectMenuClickListener(Effect effect) {
            this.effect = effect;
        }

        @Override
        public final boolean onMenuItemClick(MenuItem item) {
            return onMenuItemClick(effect, item);
        }

        public abstract boolean onMenuItemClick(Effect device, MenuItem item);
    }

    abstract class ResolutionMenuClickListener implements MenuItem.OnMenuItemClickListener {
        private VideoController.ResolutionLevel resolution = null;

        ResolutionMenuClickListener(VideoController.ResolutionLevel resolution) {
            this.resolution = resolution;
        }

        @Override
        public final boolean onMenuItemClick(MenuItem item) {
            return onMenuItemClick(resolution, item);
        }

        public abstract boolean onMenuItemClick(VideoController.ResolutionLevel resolution, MenuItem item);
    }

    abstract class MuteCameraMenuClickListener implements MenuItem.OnMenuItemClickListener {
        ooVooSdkSampleShowApp app = null;

        MuteCameraMenuClickListener(ooVooSdkSampleShowApp app) {
            this.app = app;
        }

        @Override
        public final boolean onMenuItemClick(MenuItem item) {
            return onMenuItemClick(!app.isCameraMuted(), item);
        }

        public abstract boolean onMenuItemClick(boolean state, MenuItem item);
    }

    public boolean onBackPressed() {
        app().endCall();
        app().sendEndCall();

        int count = getFragmentManager().getBackStackEntryCount();
        String name = getFragmentManager().getBackStackEntryAt(count - 2).getName();
        getFragmentManager().popBackStack(name, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        return false;
    }

    @Override
    public void onNetworkSignalStrength(int level) {
        SignalBar signalBar = (SignalBar) signalStrengthMenuItem.getActionView();
        signalBar.setLevel(level);
    }

    @Override
    public void onNetworkSecurityState(boolean isSecure)
    {
        if (isSecure) {
            secureNetworkMenuItem.setIcon(getResources().getDrawable(R.drawable.menu_ic_lock));
        } else {
            secureNetworkMenuItem.setIcon(getResources().getDrawable(R.drawable.menu_ic_lock_unlock));
        }
    }

    public void muteVideo(String userId) {
    }

    public void unmuteVideo(String userId) {
    }
    ViewGroup container;
    public void flyStars(int resId) {

        ZeroGravityAnimation animation = new ZeroGravityAnimation();
        animation.setCount(1);
        animation.setScalingFactor(0.2f);
        animation.setOriginationDirection(Direction.BOTTOM);
        animation.setDestinationDirection(Direction.TOP);
        animation.setImage(resId);
        animation.setDuration(2500);
        animation.setAnimationListener(animationSlideInListener);
        //animation.play(getActivity(),container);
        animation.play(getActivity());

    }
    Animation.AnimationListener animationSlideInListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {

        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };
}