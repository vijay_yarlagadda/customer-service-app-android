package com.oovoo.sdk.sample.ui.customerservice.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.allmysons.customeserviceapp.R;
import com.allmysons.customeserviceapp.model.Contactus;
import com.allmysons.customeserviceapp.model.ScheduleApp;

import java.util.List;

/**
 * Created by Gajjala's on 9/19/2016.
 */

public class ScheduleAppAdapter extends BaseAdapter{
    List<ScheduleApp> scheduleAppList;
    public  Context context;
    private static LayoutInflater inflater = null;

    public ScheduleAppAdapter(Context sContext, List<ScheduleApp> scheduleAppLists) {
        scheduleAppList=scheduleAppLists;
        context=sContext;
        inflater = (LayoutInflater) sContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return scheduleAppList.size();
    }

    @Override
    public Object getItem(int i) {
        return scheduleAppList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.list_items_customer, null);
        TextView time_tv = (TextView) vi.findViewById(R.id.time_tv);
        TextView date_tv = (TextView) vi.findViewById(R.id.date_tv);
        time_tv.setText(scheduleAppList.get(i).date);
        date_tv.setText(scheduleAppList.get(i).time);
        return vi;
    }
}
