package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.content.Intent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.allmysons.customeserviceapp.R;


public class AboutMe extends AppCompatActivity implements View.OnClickListener{
    TextView name, mobile, mobile_tv, mail, significant_tv, others_info_tv, save_tv;
    ImageView plus, minus, plus_img, minus_img, add_img, plus_img_et, minus_img_et;
    EditText name_et, phone_et, email_et, secondemail_tv;
    LinearLayout addemail_li, addphone_li, significant_li, significant_visible_Li;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aboutme);

        addemail_li = (LinearLayout) findViewById(R.id.addemail_li);
        addphone_li = (LinearLayout) findViewById(R.id.addphone_li);
        significant_li = (LinearLayout) findViewById(R.id.significant_li);
        significant_visible_Li = (LinearLayout) findViewById(R.id.significant_visible_Li);


        name = (TextView) findViewById(R.id.name);
        mobile = (TextView) findViewById(R.id.mobile);
        mobile_tv = (TextView) findViewById(R.id.mobile_tv);
        mail = (TextView) findViewById(R.id.mail);

        significant_tv = (TextView) findViewById(R.id.significant_tv);
        others_info_tv = (TextView) findViewById(R.id.others_info_tv);

        name_et = (EditText) findViewById(R.id.name_et);
        secondemail_tv = (EditText) findViewById(R.id.secondemail_tv);
        phone_et = (EditText) findViewById(R.id.phone_et);
        email_et = (EditText) findViewById(R.id.email_et);
        save_tv = (TextView) findViewById(R.id.save_tv);

        plus = (ImageView) findViewById(R.id.plus);
        minus = (ImageView) findViewById(R.id.minus);
        plus_img = (ImageView) findViewById(R.id.plus_img);
        minus_img = (ImageView) findViewById(R.id.minus_img);
        add_img = (ImageView) findViewById(R.id.add_img);
        plus_img_et = (ImageView) findViewById(R.id.plus_img_et);
        minus_img_et = (ImageView) findViewById(R.id.minus_img_et);

        plus.setOnClickListener(this);
        plus_img.setOnClickListener(this);
        minus.setOnClickListener(this);
        minus_img.setOnClickListener(this);
        significant_li.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.plus) {
            addphone_li.setVisibility(View.VISIBLE);

        } else if (view.getId() == R.id.plus_img) {
            addemail_li.setVisibility(View.VISIBLE);
        } else if (view.getId() == R.id.minus) {
            addphone_li.setVisibility(View.GONE);

        } else if (view.getId() == R.id.minus_img) {
            addemail_li.setVisibility(View.GONE);
        } else if (view.getId() == R.id.significant_li) {

            if (significant_visible_Li.getVisibility() == View.VISIBLE) {
                significant_visible_Li.setVisibility(View.GONE);
            } else {
                significant_visible_Li.setVisibility(View.VISIBLE);
            }


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menus; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menus, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.contactus:
                Intent contacUs = new Intent(this, ContactUs.class);
                startActivity(contacUs);
                finish();
                return true;
            case R.id.scheduleapp:
                Intent schedule = new Intent(this, ScheduleAppointment.class);
                startActivity(schedule);
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
