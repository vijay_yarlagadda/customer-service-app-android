package com.oovoo.sdk.sample.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.oovoo.sdk.sample.R;
import com.oovoo.sdk.sample.app.ApplicationSettings;
import com.oovoo.sdk.sample.app.ooVooSdkSampleShowApp;

/**
 * Created by yarlagadda on 08-11-2016.
 */

public class CallMake extends BaseFragment {

    private MenuItem settingsMenuItem = null;

    public static CallMake newInstance(MenuItem settingsMenuItem) {
        CallMake fragment = new CallMake();
        fragment.settingsMenuItem = settingsMenuItem;

        return fragment;
    }

    public CallMake() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.call_intro, container, false);
        app().makeCall();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (settingsMenuItem != null) {
            settingsMenuItem.setVisible(true);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (settingsMenuItem != null) {
            settingsMenuItem.setVisible(false);
        }
    }

    public boolean onBackPressed() {
        ((ooVooSdkSampleShowApp)getActivity().getApplication()).logout();
        return true;
    }

    public BaseFragment getBackFragment()
    {
        return CallIntro.newInstance(settingsMenuItem);
    }
}

