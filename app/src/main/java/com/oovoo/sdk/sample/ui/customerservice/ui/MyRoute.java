package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.allmysons.customeserviceapp.R;

/**
 * Created by Gajjala's on 10/20/2016.
 */

public class MyRoute extends AppCompatActivity implements View.OnClickListener {

    LinearLayout ofc_addres_LI, stop1_LI, stop2_LI, stop3_LI;
    TextView addStop;
    ImageView googlemaps_imv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myroute);


        ofc_addres_LI = (LinearLayout) findViewById(R.id.ofc_addres_LI);
        stop1_LI = (LinearLayout) findViewById(R.id.stop1_LI);
        stop2_LI = (LinearLayout) findViewById(R.id.stop2_LI);
        stop3_LI = (LinearLayout) findViewById(R.id.stop3_LI);
        googlemaps_imv = (ImageView) findViewById(R.id.googlemaps_imv);
        addStop = (TextView) findViewById(R.id.add_stop_tv);


        ofc_addres_LI.setOnClickListener(this);
        stop1_LI.setOnClickListener(this);
        stop2_LI.setOnClickListener(this);
        stop3_LI.setOnClickListener(this);
        googlemaps_imv.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.ofc_addres_LI) {
            Intent myroute = new Intent(MyRoute.this, EditLocation.class);
            startActivity(myroute);
        } else if (view.getId() == R.id.googlemaps_imv) {
            Intent edit = new Intent(MyRoute.this, GoogleMaps.class);
            startActivity(edit);
        } else {
            Intent edit = new Intent(MyRoute.this, EditLocation.class);
            startActivity(edit);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.callbutton_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.call_btn:
                Intent intent = new Intent(this, Callintro.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
