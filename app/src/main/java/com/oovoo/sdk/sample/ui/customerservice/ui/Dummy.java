package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.allmysons.customeserviceapp.R;

/**
 * Created by Gajjala's on 9/27/2016.
 */

public class Dummy extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dummy);
    }
    }
