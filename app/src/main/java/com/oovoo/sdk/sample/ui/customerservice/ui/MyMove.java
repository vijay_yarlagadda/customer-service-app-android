package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.allmysons.customeserviceapp.R;

/**
 * Created by Gajjala's on 9/26/2016.
 */

public class MyMove extends Fragment implements View.OnClickListener{
    LinearLayout aboutme,myroute,itemsmove,agrements,servicesincluded,servicesnotincluded,servicesinc_listview;
    TextView more;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        //Inflate the layout for this fragment
        View view= inflater.inflate(
                R.layout.mymove, container, false);

        aboutme=(LinearLayout)view.findViewById(R.id.aboutme_li);

        servicesinc_listview=(LinearLayout)view.findViewById(R.id.servicesinc_LI);

        myroute=(LinearLayout)view.findViewById(R.id.myroute_li);
        itemsmove=(LinearLayout)view.findViewById(R.id.itemsmove_li);
        agrements=(LinearLayout)view.findViewById(R.id.agrements_li);
        servicesincluded=(LinearLayout)view.findViewById(R.id.serve_inc_tv);
        servicesnotincluded=(LinearLayout)view.findViewById(R.id.services_not_inc);
        more=(TextView)view.findViewById(R.id.more_textView);

        aboutme.setOnClickListener(this);
        myroute.setOnClickListener(this);
        itemsmove.setOnClickListener(this);
        agrements.setOnClickListener(this);
        servicesincluded.setOnClickListener(this);
        servicesnotincluded.setOnClickListener(this);

        servicesinc_listview.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.aboutme_li){
            Intent aboutme=new Intent(getActivity(),AboutMe.class);
            startActivity(aboutme);
        }else if(view.getId()==R.id.itemsmove_li){
            Intent itemsmove=new Intent(getActivity(),ItemsMoving.class);
            startActivity(itemsmove);
        }else if(view.getId()==R.id.servicesinc_LI) {
            Intent services = new Intent(getActivity(),ServiceIncluded.class);
            startActivity(services);
        }else if(view.getId()==R.id.services_not_inc) {
            Intent servicesnotincluded = new Intent(getActivity(), ServicesnotIncluded.class);
            startActivity(servicesnotincluded);
        }else if(view.getId()==R.id.myroute_li) {
            Intent myroute = new Intent(getActivity(), MyRoute.class);
            startActivity(myroute);
        }else {
            Intent dummy=new Intent(getActivity(),Dummy.class);
            startActivity(dummy);
        }
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.callbutton_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.call_btn:
                // Do Fragment menu item stuff here

                Intent intent = new Intent(getActivity(), Callintro.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            default:
                break;
        }

        return false;
    }
}
