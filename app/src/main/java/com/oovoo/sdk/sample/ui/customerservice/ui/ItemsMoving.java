package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.allmysons.customeserviceapp.Adapter.ItemsMovingAdapter;
import com.allmysons.customeserviceapp.R;
import com.allmysons.customeserviceapp.model.Itemsmoving;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gajjala's on 9/26/2016.
 */

public class ItemsMoving extends AppCompatActivity implements AdapterView.OnItemClickListener,View.OnClickListener{
    ListView listView;
    ImageView add_img;
    Dialog alertDialog;
    ItemsMovingAdapter itemsMovingAdapter;
    List<Itemsmoving> itemsmovingList;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.items_listview);

        add_img=(ImageView)findViewById(R.id.add_img);

        listView=(ListView)findViewById(R.id.items_lits);
        itemsmovingList=new ArrayList<>();

        itemsmovingList.add(new Itemsmoving(R.drawable.bed,"Master BedRoom",R.drawable.more,R.drawable.error_default));
        itemsmovingList.add(new Itemsmoving(R.drawable.office,"Office",R.drawable.more,R.drawable.error_default));
        itemsmovingList.add(new Itemsmoving(R.drawable.kitchen,"Kitchen",R.drawable.more,R.drawable.error_default));
        itemsmovingList.add(new Itemsmoving(R.drawable.solarium,"Solarium",R.drawable.more,R.drawable.error_default));

        itemsMovingAdapter=new ItemsMovingAdapter(this,itemsmovingList);
        listView.setAdapter(itemsMovingAdapter);
        listView.setOnItemClickListener(this);

      add_img.setOnClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

      //  Intent
        Intent listView=new Intent(this,ItemsSub.class);
        startActivity(listView);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.add_img) {

            //add_img.setImageResource(R.drawable.call);
            alertDialog = new Dialog(this);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            LayoutInflater inflater = getLayoutInflater();

            View alertLayout = inflater.inflate(R.layout.add_symbol, null);
            alertDialog.setContentView(alertLayout);


            EditText enter_et = (EditText) alertLayout.findViewById(R.id.enter_et);
            TextView addroom_tv = (TextView) alertLayout.findViewById(R.id.addroom_tv);

            addroom_tv.setOnClickListener(this);


            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();

        } else if (view.getId() == R.id.addroom_tv) {

            if(alertDialog==null){

            }else {
                alertDialog.dismiss();
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.callbutton_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.call_btn:
                Intent intent = new Intent(this, Callintro.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}


