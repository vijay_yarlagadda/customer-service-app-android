package com.oovoo.sdk.sample.ui.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.oovoo.sdk.sample.R;
import com.oovoo.sdk.sample.app.ooVooSdkSampleShowApp;
import com.oovoo.sdk.sample.call.CNMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yarlagadda on 02-12-2016.
 */

public class CallScreenFragment extends BaseFragment implements View.OnClickListener, ooVooSdkSampleShowApp.CallNegotiationListener {

    /*ImageView image1, image2;
    Animation animationSlideDownIn, animationSlideDownOut;*/
    ImageView curSlidingImage;
    private static final String KEY_ADAPTER_STATE = "com.oovoo.sdk.fragmentstate.KEY_ADAPTER_STATE";
    private static final int MAX_CALL_RECEIVERS = 4;
    private ListView callReceiversList = null;
    private CallScreenFragment.CallReceiverAdapter callReceiverAdapter = null;
    private AlertDialog callDialogBuilder = null;
    private AlertDialog callReceiverDialog = null;
    private ArrayList<CallScreenFragment.CallReceiverAdapter.CallReceiver> adapterSavedState = null;
    private int count = 0;
    private int enabledReceivesCount = 0;
    private MenuItem settingsMenuItem = null;

    public static final CallScreenFragment newInstance(MenuItem settingsMenuItem) {
        CallScreenFragment fragment = new CallScreenFragment();
        fragment.settingsMenuItem = settingsMenuItem;
        return fragment;
    }

    public CallScreenFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_ADAPTER_STATE)) {
            adapterSavedState = savedInstanceState.getParcelableArrayList(KEY_ADAPTER_STATE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.call_intro, container, false);
        callReceiverAdapter = new CallScreenFragment.CallReceiverAdapter();
        callReceiverAdapter.addItem("999999");
        ImageView callButton = (ImageView) view.findViewById(R.id.phone_green);
        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app().generateConferenceId();
                enabledReceivesCount = callReceiverAdapter.getEnabledReceivesCount();
                boolean showDialog = sendCNMessage(CNMessage.CNMessageType.Calling, 30000, new ooVooSdkSampleShowApp.MessageCompletionHandler() { // Timeout 30 sec
                    @Override
                    public void onHandle(final boolean state) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!state) {
                                    count = 0 ;
                                    Toast.makeText(getActivity(), R.string.fail_to_send_message, Toast.LENGTH_LONG).show();
                                    callDialogBuilder.hide();
                                    return  ;
                                }

                                count = enabledReceivesCount;
                            }
                        });

                    }
                });

                if (showDialog) {
                    callDialogBuilder.show();
                } else {
                    Toast.makeText(getActivity(), R.string.no_receivers, Toast.LENGTH_LONG).show();
                }
            }
        });

        callDialogBuilder = new AlertDialog.Builder(getActivity()).create();
        View outgoingCallDialog = inflater.inflate(R.layout.outgoing_call_dialog, null);
        outgoingCallDialog.setAlpha(0.5f);
        callDialogBuilder.setView(outgoingCallDialog);
        Button cancelButton = (Button) outgoingCallDialog.findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(this);
        callDialogBuilder.setCancelable(false);
        app().addCallNegotiationListener(this);

        return view;
    }
    public void externalCallFunction(){
        app().generateConferenceId();
        enabledReceivesCount = callReceiverAdapter.getEnabledReceivesCount();
        boolean showDialog = sendCNMessage(CNMessage.CNMessageType.Calling, 30000, new ooVooSdkSampleShowApp.MessageCompletionHandler() { // Timeout 30 sec
            @Override
            public void onHandle(final boolean state) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!state) {
                            count = 0 ;
                            Toast.makeText(getActivity(), R.string.fail_to_send_message, Toast.LENGTH_LONG).show();
                            callDialogBuilder.hide();
                            return  ;
                        }

                        count = enabledReceivesCount;
                    }
                });

            }
        });

        if (showDialog) {
            callDialogBuilder.show();
        } else {
            Toast.makeText(getActivity(), R.string.no_receivers, Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        /*image1.clearAnimation();
        image2.clearAnimation();
*/
    }

    /*Animation.AnimationListener animationSlideInListener
            = new Animation.AnimationListener(){

        @Override
        public void onAnimationEnd(Animation arg0) {
            // TODO Auto-generated method stub
            if(curSlidingImage == image1){
                image1.startAnimation(animationSlideDownOut);
            }else {
                image2.startAnimation(animationSlideDownOut);
            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onAnimationStart(Animation animation) {
            // TODO Auto-generated method stub

        }

    };

    Animation.AnimationListener animationSlideOutListener
            = new Animation.AnimationListener(){

        @Override
        public void onAnimationEnd(Animation animation) {
            // TODO Auto-generated method stub
            if(curSlidingImage == image1){


            }else if(curSlidingImage == image2){


            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onAnimationStart(Animation animation) {
            // TODO Auto-generated method stub

        }

    };*/
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        getActivity().getWindow().setBackgroundDrawableResource(R.drawable.slqsm);
    }


    @Override
    public void onDestroyView()
    {
        if (callReceiverDialog != null) {
            callReceiverDialog.getWindow().setSoftInputMode(0);
            callReceiverDialog.hide();
        }
        callDialogBuilder.hide();
        app().removeCallNegotiationListener(this);
        super.onDestroyView();
    }

    private static class CallReceiverAdapter extends BaseAdapter
    {
        private final List<CallScreenFragment.CallReceiverAdapter.CallReceiver> receivers = new ArrayList<CallScreenFragment.CallReceiverAdapter.CallReceiver>();

        private class CallReceiverViewHolder
        {
            TextView textView;
            Switch callSwitch;
        }

        public static class CallReceiver implements Parcelable {
            private String receiverId = null;
            private Boolean isCallEnabled = true;

            public CallReceiver(String receiverId) {
                this.receiverId = receiverId;
            }

            public String getReceiverId() {
                return receiverId;
            }

            public void setCallEnabled(boolean isCallEnabled) {
                this.isCallEnabled = isCallEnabled;
            }

            public boolean isCallEnabled() {
                return this.isCallEnabled;
            }

            protected CallReceiver(Parcel in) {
                receiverId = in.readString();
                isCallEnabled = in.readByte() != 0;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(receiverId);
                dest.writeByte((byte) (isCallEnabled ? 1 : 0));
            }

            @SuppressWarnings("unused")
            public static final Parcelable.Creator<CallScreenFragment.CallReceiverAdapter.CallReceiver> CREATOR = new Parcelable.Creator<CallScreenFragment.CallReceiverAdapter.CallReceiver>() {
                @Override
                public CallScreenFragment.CallReceiverAdapter.CallReceiver createFromParcel(Parcel in) {
                    return new CallScreenFragment.CallReceiverAdapter.CallReceiver(in);
                }

                @Override
                public CallScreenFragment.CallReceiverAdapter.CallReceiver[] newArray(int size) {
                    return new CallScreenFragment.CallReceiverAdapter.CallReceiver[size];
                }
            };
        }

        ArrayList<CallScreenFragment.CallReceiverAdapter.CallReceiver> onSaveInstanceState()
        {
            int size = getCount();
            ArrayList<CallScreenFragment.CallReceiverAdapter.CallReceiver> items = new ArrayList<CallScreenFragment.CallReceiverAdapter.CallReceiver>(size);
            for(int i = 0; i < size; i++) {
                items.add(getItem(i));
            }
            return items;
        }

        void onRestoreInstanceState(ArrayList<CallScreenFragment.CallReceiverAdapter.CallReceiver> items)
        {
            receivers.clear();
            receivers.addAll(items);
        }

        public int getEnabledReceivesCount()
        {
            int count = 0;
            for (int i = 0; i < receivers.size(); i++) {
                CallScreenFragment.CallReceiverAdapter.CallReceiver receiver = getItem(i);
                if (receiver.isCallEnabled() && count < MAX_CALL_RECEIVERS) {
                    count++;
                }
            }
            return count;
        }

        @Override
        public int getCount()
        {
            return receivers.size();
        }

        @Override
        public CallScreenFragment.CallReceiverAdapter.CallReceiver getItem(int i)
        {
            return receivers.get(i);
        }

        @Override
        public long getItemId(int i)
        {
            return i; // index number
        }

        @Override
        public View getView(int index, View view, final ViewGroup parent)
        {
            CallScreenFragment.CallReceiverAdapter.CallReceiverViewHolder viewHolder = null;
            final CallScreenFragment.CallReceiverAdapter.CallReceiver receiver = receivers.get(index);
            if (view == null)
            {
                viewHolder = new CallScreenFragment.CallReceiverAdapter.CallReceiverViewHolder();

                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                view = inflater.inflate(R.layout.call_receiver_info, parent, false);
                viewHolder.textView = (TextView) view.findViewById(R.id.receiver_id_text_view);
                viewHolder.callSwitch = (Switch) view.findViewById(R.id.receiver_switch);

                view.setTag(viewHolder);
            }
            else
            {
                viewHolder = (CallScreenFragment.CallReceiverAdapter.CallReceiverViewHolder) view.getTag();
            }

            viewHolder.textView.setText(receiver.getReceiverId());

            viewHolder.callSwitch.setOnCheckedChangeListener(null);
            viewHolder.callSwitch.setChecked(receiver.isCallEnabled());
            viewHolder.callSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    receiver.setCallEnabled(isChecked);
                }
            });

            return view;
        }

        public void addItem(String receiverId)
        {
            if (receiverId == null || receiverId.isEmpty())
                return;

            receivers.add(new CallScreenFragment.CallReceiverAdapter.CallReceiver(receiverId));
        }
    }

    @Override
    public void onClick(View v) {

        callDialogBuilder.hide();

        switch (v.getId()) {
            case R.id.cancel_button:
            {
                sendCNMessage(CNMessage.CNMessageType.Cancel, null);
            }
            break;

            default:
                break;
        }
    }

    private boolean sendCNMessage(CNMessage.CNMessageType type, long timeout, ooVooSdkSampleShowApp.MessageCompletionHandler completionHandler)
    {
        ArrayList<String> toList = new ArrayList<String>();
        for (int i = 0; i < callReceiverAdapter.getCount(); i++) {
            CallScreenFragment.CallReceiverAdapter.CallReceiver receiver = (CallScreenFragment.CallReceiverAdapter.CallReceiver) callReceiverAdapter.getItem(i);

            if (receiver.isCallEnabled() && toList.size() < MAX_CALL_RECEIVERS) {
                toList.add(receiver.getReceiverId());
            }
        }

        if (toList.isEmpty()) {
            return false;
        }

        app().sendCNMessage(toList, type,timeout, completionHandler);

        return true;
    }

    private boolean sendCNMessage(CNMessage.CNMessageType type, ooVooSdkSampleShowApp.MessageCompletionHandler completionHandler)
    {
        return sendCNMessage(type,0,completionHandler);
    }

    @Override
    public void onMessageReceived(CNMessage cnMessage)
    {
        if (app().getUniqueId().equals(cnMessage.getUniqueId())) {
            return;
        }

        if (cnMessage.getMessageType() == CNMessage.CNMessageType.AnswerAccept) {
            app().join(app().getConferenceId(), true);
        } else if (cnMessage.getMessageType() == CNMessage.CNMessageType.AnswerDecline) {
            count--;
            if (count <= 0) {
                callDialogBuilder.hide();
            }
        }  else if (cnMessage.getMessageType() == CNMessage.CNMessageType.Busy) {
            count--;
            if (count <= 0) {
                callDialogBuilder.hide();
            }
        }
    }

    public BaseFragment getBackFragment() {
        return CallIntro.newInstance(settingsMenuItem);
    }
}