package com.oovoo.sdk.sample.ui.customerservice.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.allmysons.customeserviceapp.R;
import com.allmysons.customeserviceapp.model.Listmodels;

import java.util.List;

/**
 * Created by Gajjala's on 9/29/2016.
 */

public class ListModelsAdapter extends BaseAdapter {

    List<Listmodels> listmodels;
    public Context context;
    private static LayoutInflater inflater = null;

    public ListModelsAdapter(Context sContext, List<Listmodels> slistmodels) {
        listmodels=slistmodels;
        context=sContext;
        inflater = (LayoutInflater) sContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return listmodels.size();
    }

    @Override
    public Object getItem(int i) {
        return listmodels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.items_moving_listitems, null);

        TextView time_tv = (TextView) vi.findViewById(R.id.bedroom_tv);
        ImageView  imageView = (ImageView) vi.findViewById(R.id.bed_img);
        ImageView  imageView2 = (ImageView) vi.findViewById(R.id.bed_more);
        ImageView  imageView3= (ImageView) vi.findViewById(R.id.bed_error);

        time_tv.setText(listmodels.get(i).source);
      imageView.setImageResource(listmodels.get(i).number);
         imageView2.setImageResource(listmodels.get(i).star);
        //imageView3.setImageResource(listmodels.get(i).error);

        return vi;
    }
}
