package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.allmysons.customeserviceapp.R;

/**
 * Created by Gajjala's on 10/15/2016.
 */

public class ServicesnotIncluded extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    LinearLayout standard_unpacking_LI,request_LI;
    TextView request_tv;
    ImageView down_arrow,exclamation;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.services_not_included);

        standard_unpacking_LI = (LinearLayout) findViewById(R.id.standard_unpacking_LI);
        request_LI = (LinearLayout) findViewById(R.id.request_LI);
        down_arrow = (ImageView) findViewById(R.id.down_arrow);
        exclamation = (ImageView) findViewById(R.id.exclamation);
        request_tv = (TextView) findViewById(R.id.request_tv);


        standard_unpacking_LI.setOnClickListener(this);
        down_arrow.setOnClickListener(this);
        request_tv.setOnClickListener(this);
        exclamation.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {


        if(view.getId()==R.id.standard_unpacking_LI){
            if(request_LI.getVisibility()==View.VISIBLE){
                request_LI.setVisibility(View.GONE);
                standard_unpacking_LI.setBackgroundColor(Color.parseColor("#ffffff"));
                down_arrow.setImageResource(R.drawable.downarow_default);

            }else {
                request_LI.setVisibility(View.VISIBLE);
                down_arrow.setImageResource(R.drawable.uparrow_white);
                standard_unpacking_LI.setBackgroundColor(Color.parseColor("#9EA8B4"));

            }

        }else if(view.getId()==R.id.request_tv){
            Intent request_tv = new Intent(ServicesnotIncluded.this,Request.class);
            startActivity(request_tv);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent request=new Intent(this,Request.class);
        startActivity(request);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.callbutton_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.call_btn:
                Intent intent = new Intent(this, Callintro.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
