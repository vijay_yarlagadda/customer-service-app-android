package com.oovoo.sdk.sample.ui.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.oovoo.sdk.sample.R;

/**
 * Created by yarlagadda on 08-11-2016.
 */

public class CallIntro extends BaseFragment {

    private static final int MIN_USERNAME_LENGTH = 6;
    private static final int USERNAME_LIMIT = 200;
    private static final int DISPLAY_NAME_LIMIT = 100;
    private String errorDescription = null;
    private EditText usernameEditText = null;
    private EditText displayNameEditText = null;
    private TextView errorTextView = null;
    private MenuItem settingsMenuItem = null;

    private EditText code = null;

    public CallIntro() {
    }

    public static CallIntro newInstance(MenuItem settingsMenuItem) {
        CallIntro fragment = new CallIntro();
        fragment.settingsMenuItem = settingsMenuItem;

        return fragment;
    }

    public static final CallIntro newInstance(String errorDescription) {
        CallIntro instance = new CallIntro();
        instance.setErrorDescription(errorDescription);
        return instance;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        getActivity().getWindow().setBackgroundDrawableResource(R.drawable.slqsm);
        if (settingsMenuItem != null) {
            settingsMenuItem.setVisible(true);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        if (settingsMenuItem != null) {
            settingsMenuItem.setVisible(false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login, container, false);
        /*onLoginClick();
        if (!app().isOnline()) {
           // errorTextView.setVisibility(View.VISIBLE);
            //errorTextView.setText(getActivity().getResources().getString(R.string.no_internet));
        } else if (errorDescription != null) {
            //errorTextView.setVisibility(View.VISIBLE);
            //errorTextView.setText(errorDescription);
        }*/
        code = (EditText) view.findViewById(R.id.code_et);
        code.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() == 6) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(code.getWindowToken(), 0);
                    app().login(code.getText().toString(), "Vijay");
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        return view;
    }

    public void onLoginClick() {
        errorTextView.setText("");
        String username = usernameEditText.getText().toString();
        if (username.isEmpty()) {
            showErrorMessageBox(getString(R.string.login_title), getString(R.string.enter_username_toast));

            return;
        }

        if (username.length() < MIN_USERNAME_LENGTH) {
            showErrorMessageBox(getString(R.string.characters_missing), getString(R.string.min_username_length));

            return;
        }

        String displayName = displayNameEditText.getText().toString();

        if (!checkDisplayName(displayName)) {
            return;
        }

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(usernameEditText.getWindowToken(), 0);

        app().login(usernameEditText.getText().toString(), displayName);
    }

    private boolean checkDisplayName(String displayName) {
        if (displayName.isEmpty()) {
            showErrorMessageBox(getString(R.string.login_title), getString(R.string.enter_conference_display_name));

            return false;
        }

        if (displayName.length() > DISPLAY_NAME_LIMIT) {
            showErrorMessageBox(getString(R.string.login_title), getString(R.string.display_name_too_long));

            return false;
        }

        return true;
    }

    public void showErrorMessageBox(String title, String msg) {
        try {
            AlertDialog.Builder popupBuilder = new AlertDialog.Builder(getActivity());
            TextView myMsg = new TextView(getActivity());
            myMsg.setText(msg);
            myMsg.setGravity(Gravity.CENTER);
            popupBuilder.setTitle(title);
            popupBuilder.setPositiveButton("OK", null);
            popupBuilder.setView(myMsg);

            popupBuilder.show();
        } catch (Exception e) {
        }
    }

    /***
     * In the fragment when user click on back button we just call finish on host activity
     */
    public boolean onBackPressed() {
        this.getActivity().finish();
        return false;
    }

}
