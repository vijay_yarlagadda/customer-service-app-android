package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.allmysons.customeserviceapp.Adapter.ContactUsAdapter;
import com.allmysons.customeserviceapp.R;
import com.allmysons.customeserviceapp.model.Contactus;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gajjala's on 9/17/2016.
 */

public class ContactUs extends AppCompatActivity {

    List<Contactus> contactusList;
    ListView contactListview;
    ContactUsAdapter contactUsAdapter;
    EditText typemessage_ev;
    ImageView send_tv;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contactus);
        contactListview = (ListView) findViewById(R.id.listview);
        typemessage_ev=(EditText)findViewById(R.id.typemessage_ev);

        send_tv=(ImageView) findViewById(R.id.send_tv);
        contactusList = new ArrayList<>();

        contactUsAdapter = new ContactUsAdapter(this, contactusList);
        contactListview.setAdapter(contactUsAdapter);
        // ATTENTION: This was auto-generated to implement the App Indexing API.

        send_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                contactusList.add(new Contactus(null,null,typemessage_ev.getText().toString(),true));
                contactUsAdapter.notifyDataSetChanged();
                typemessage_ev.setText("");
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menus; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menunew, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.aboutus:
                Intent contacUs = new Intent(this, AboutMe.class);
                startActivity(contacUs);
                finish();
                return true;
            case R.id.scheduleapp:
                Intent schedule = new Intent(this, ScheduleAppointment.class);
                startActivity(schedule);
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

}
