package com.oovoo.sdk.sample.ui.customerservice.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.allmysons.customeserviceapp.R;
import com.allmysons.customeserviceapp.model.Contactus;

import java.util.List;

/**
 * Created by Gajjala's on 9/19/2016.
 */

public class ContactUsAdapter extends BaseAdapter{
    List<Contactus> contactusListData;
    public  Context context;
    private static LayoutInflater inflater = null;

    public ContactUsAdapter(Context sContext, List<Contactus> contactusList) {
        contactusListData=contactusList;
        context=sContext;
        inflater = (LayoutInflater) sContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return contactusListData.size();
    }

    @Override
    public Object getItem(int i) {
        return contactusListData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.contactus_listitems, null);
        TextView reciver = (TextView) vi.findViewById(R.id.reciver_tv);
        TextView sender = (TextView) vi.findViewById(R.id.sender_tv);
        LinearLayout sender_LI = (LinearLayout) vi
                .findViewById(R.id.sender_LI);
        LinearLayout reciver_LI = (LinearLayout) vi
                .findViewById(R.id.reciver_LI);
        if(contactusListData.get(i).sender==true){
            sender_LI.setVisibility(View.VISIBLE);
            reciver_LI.setVisibility(View.GONE);
            sender.setText(contactusListData.get(i).text);
        }else{
            sender_LI.setVisibility(View.GONE);
            reciver_LI.setVisibility(View.VISIBLE);
            reciver.setText(contactusListData.get(i).text);
        }

        return vi;
    }
}
