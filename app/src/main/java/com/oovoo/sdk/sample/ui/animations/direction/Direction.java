package com.oovoo.sdk.sample.ui.animations.direction;

/**
 * Created by  Harish Sridharan on July 20 2015
 */
public enum Direction {

    TOP, LEFT, RIGHT, BOTTOM,RANDOM
}
