package com.oovoo.sdk.sample.ui.customerservice.model;

/**
 * Created by Gajjala's on 9/29/2016.
 */

public class Listmodels {
  public   String source;
    public int star;
    public int error;
    public  int number;

    public Listmodels(String source, int star, int error, int number) {
        this.source = source;
        this.star = star;
        this.error = error;
        this.number = number;
    }
}
