package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.allmysons.customeserviceapp.Adapter.ScheduleAppAdapter;
import com.allmysons.customeserviceapp.R;
import com.allmysons.customeserviceapp.model.*;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gajjala's on 9/17/2016.
 */

public class ScheduleAppointment extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemClickListener {

    List<ScheduleApp> scheduleAppList;
    ListView shceduleListview;
    ScheduleAppAdapter scheduleAppAdapter;
    EditText typemessage_ev;
    ImageView send_tv;
    TextView today_tv,tomorrow_tv,dayaftertmrw_tv,nextdayafter_tv;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scheduleappointment);

         today_tv = (TextView) findViewById(R.id.today_tv);
         tomorrow_tv = (TextView) findViewById(R.id.tmrw_tv);
         dayaftertmrw_tv = (TextView) findViewById(R.id.daytmrw_tv);
         nextdayafter_tv = (TextView) findViewById(R.id.nextdaytmrew_tv);

        today_tv.setOnClickListener(this);
        tomorrow_tv.setOnClickListener(this);
        dayaftertmrw_tv.setOnClickListener(this);
        nextdayafter_tv.setOnClickListener(this);



        shceduleListview = (ListView) findViewById(R.id.listview);
        scheduleAppList = new ArrayList<>();

        scheduleAppAdapter = new ScheduleAppAdapter(this, scheduleAppList);
        shceduleListview.setAdapter(scheduleAppAdapter);
        scheduleAppList.add(new ScheduleApp(null,"Mon-10:00 AM","sept 2016"));
        scheduleAppList.add(new ScheduleApp(null,"Mon-11:00 AM","sept 2016"));
        scheduleAppList.add(new ScheduleApp(null,"Mon-12:00 AM","sept 2016"));
        scheduleAppList.add(new ScheduleApp(null,"Mon-01:00 PM","sept 2016"));
        scheduleAppList.add(new ScheduleApp(null,"Mon-02:00 PM","sept 2016"));

        shceduleListview.setOnItemClickListener(this);
    }

    public  void showAlertOnly(final Context context, String title,
                                     final String message, String positiveButton) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.YourDialogStyle); // Setting
        // Title
        TextView titlee = new TextView(context);

        // You Can Customise your Title here
        titlee.setText("Schedule Appointment");
        titlee.setPadding(10, 10, 10, 10);
        titlee.setGravity(Gravity.CENTER);
        titlee.setTextColor(Color.RED);
        titlee.setTextSize(18);
        builder.setCustomTitle(titlee);
        builder.setMessage(message);
        builder.setNegativeButton(positiveButton,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {



                        dialog.cancel();

                    }
                });
        // this will solve your error
        AlertDialog alert = builder.create();
        alert.show();
        alert.getWindow().getAttributes();

        TextView textView = (TextView) alert.findViewById(android.R.id.message);
        textView.setTextSize(16);
        Button btn1 = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        btn1.setTextSize(16);
    }

    @Override
    public void onClick(View view) {

        if(view.getId()==R.id.today_tv){
            today_tv.setBackgroundResource(R.drawable.bottom_line_blue);
            //today_tv.setTextColor(Color.parseColor("#3F51B5"));
            nextdayafter_tv.setBackgroundColor(Color.parseColor("#F7F7F7"));
            dayaftertmrw_tv.setBackgroundColor(Color.parseColor("#F7F7F7"));
            tomorrow_tv.setBackgroundColor(Color.parseColor("#F7F7F7"));

        }
        else if(view.getId()==R.id.tmrw_tv){
            tomorrow_tv.setBackgroundResource(R.drawable.bottom_line_blue);
            today_tv.setBackgroundColor(Color.parseColor("#F7F7F7"));
            dayaftertmrw_tv.setBackgroundColor(Color.parseColor("#F7F7F7"));
            nextdayafter_tv.setBackgroundColor(Color.parseColor("#F7F7F7"));
        }
        else if(view.getId()==R.id.daytmrw_tv){
            dayaftertmrw_tv.setBackgroundResource(R.drawable.bottom_line_blue);
            nextdayafter_tv.setBackgroundColor(Color.parseColor("#F7F7F7"));
            today_tv.setBackgroundColor(Color.parseColor("#F7F7F7"));
            tomorrow_tv.setBackgroundColor(Color.parseColor("#F7F7F7"));
        }
        if(view.getId()==R.id.nextdaytmrew_tv){
            nextdayafter_tv.setBackgroundResource(R.drawable.bottom_line_blue);
            tomorrow_tv.setBackgroundColor(Color.parseColor("#F7F7F7"));
            dayaftertmrw_tv.setBackgroundColor(Color.parseColor("#F7F7F7"));
            today_tv.setBackgroundColor(Color.parseColor("#F7F7F7"));
        }
    }
   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menus; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menun, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.contactus:
                Intent contacUs = new Intent(this, ContactUs.class);
                startActivity(contacUs);
                finish();
                return true;
            case R.id.aboutus:
                Intent schedule = new Intent(this, AboutMe.class);
                startActivity(schedule);
                finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
*/
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        Intent schedule=new Intent(ScheduleAppointment.this,NavigationActivity.class);
        startActivity(schedule);

       // showAlertOnly(this,"titlee","Your date is confirmed successfully","Ok");
    }
}
