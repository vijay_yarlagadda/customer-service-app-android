package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.allmysons.customeserviceapp.R;

/**
 * Created by Gajjala's on 11/1/2016.
 */

public class GypsyMoth extends AppCompatActivity implements View.OnClickListener {
TextView doitmyself;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gypsy_moth);

        doitmyself=(TextView)findViewById(R.id.doit_myself_tv);

        doitmyself.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.doit_myself_tv);
        Intent gypsy=new Intent(GypsyMoth.this,Gypsy.class);
        startActivity(gypsy);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.callbutton_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.call_btn:
                Intent intent = new Intent(this, Callintro.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
