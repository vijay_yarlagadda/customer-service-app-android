package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.allmysons.customeserviceapp.R;

/**
 * Created by Gajjala's on 10/5/2016.
 */

public class SplashScreen extends AppCompatActivity {
SharedPreferences userLogin;
    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 3000;

    /** Called when the activity is first created. */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);

        userLogin=getSharedPreferences("user", Context.MODE_PRIVATE);
        final String userLoginString=userLogin.getString("user","");

         /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                if (userLoginString.length()>=1){
                    Intent splash=new Intent(SplashScreen.this,Callintro.class);
                    startActivity(splash);
                }else {
                    Intent mainIntent = new Intent(SplashScreen.this,Login.class);
                    startActivity(mainIntent);
                }

                finish();
            }
        },SPLASH_DISPLAY_LENGTH);
    }

}

