package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.allmysons.customeserviceapp.R;

import static com.allmysons.customeserviceapp.R.id.down_arrow;

/**
 * Created by Gajjala's on 10/20/2016.
 */

public class EditLocation extends AppCompatActivity implements View.OnClickListener {

    LinearLayout hometype_LI,select_hometype_LI;
    ImageView downarow_default;
    ToggleButton toggleButton;
    TextView single_tv,town,condo,appartment,storage,others;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_location);

       single_tv=(TextView)findViewById(R.id.single_tv);
        town=(TextView)findViewById(R.id.town_home_tv);
        condo=(TextView)findViewById(R.id.condo_tv);
        appartment=(TextView)findViewById(R.id.appartment_tv);
        storage=(TextView)findViewById(R.id.storage_tv);
        others=(TextView)findViewById(R.id.others_tv);

        downarow_default=(ImageView)findViewById(R.id.downarow_default);
        hometype_LI=(LinearLayout)findViewById(R.id.hometype_LI);
        select_hometype_LI=(LinearLayout)findViewById(R.id.select_hometype_LI);
        hometype_LI.setOnClickListener(this);
        select_hometype_LI.setOnClickListener(this);
        downarow_default.setOnClickListener(this);

        single_tv.setOnClickListener(this);
        town.setOnClickListener(this);
        condo.setOnClickListener(this);
        appartment.setOnClickListener(this);
        storage.setOnClickListener(this);
        others.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if ((view.getId()==R.id.single_tv)|| (view.getId()==R.id.town_home_tv) || (view.getId()==R.id.condo_tv)
                || (view.getId()==R.id.appartment_tv) ||(view.getId()==R.id.storage_tv) ||(view.getId()==R.id.others_tv) ){
            select_hometype_LI.setVisibility(View.GONE);

            hometype_LI.setBackgroundColor(Color.parseColor("#ffffff"));
            downarow_default.setImageResource(R.drawable.downarow_default);

        }else if((view.getId()==R.id.town_home_tv)|| (view.getId()==R.id.town_home_tv) || (view.getId()==R.id.condo_tv)
                || (view.getId()==R.id.appartment_tv) ||(view.getId()==R.id.storage_tv) ||(view.getId()==R.id.others_tv) ){
            select_hometype_LI.setVisibility(View.GONE);

            hometype_LI.setBackgroundColor(Color.parseColor("#9EA8B4"));
            downarow_default.setImageResource(R.drawable.downarow_white);
        }
        if (view.getId()==R.id.hometype_LI){
            if (select_hometype_LI.getVisibility()==View.VISIBLE){
                select_hometype_LI.setVisibility(View.GONE);
                hometype_LI.setBackgroundColor(Color.parseColor("#ffffff"));
                downarow_default.setImageResource(R.drawable.downarow_default);

            }else {
                select_hometype_LI.setVisibility(View.VISIBLE);
                downarow_default.setImageResource(R.drawable.downarow_white);
                hometype_LI.setBackgroundColor(Color.parseColor("#9EA8B4"));

            }
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.callbutton_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.call_btn:
                Intent intent = new Intent(this, Callintro.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
