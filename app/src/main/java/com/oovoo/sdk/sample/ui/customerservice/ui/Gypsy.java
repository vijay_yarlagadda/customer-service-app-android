package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.allmysons.customeserviceapp.Adapter.GypsyAdapter;
import com.allmysons.customeserviceapp.R;
import com.allmysons.customeserviceapp.model.GypsyModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gajjala's on 11/2/2016.
 */

public class Gypsy extends AppCompatActivity implements View.OnClickListener {
    List<GypsyModel> gypsyModels;
    ListView listView;
    GypsyAdapter gypsyAdapter;
    TextView textView, textview1;
    ImageView imageView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gypsymoth_checklist);

        imageView = (ImageView) findViewById(R.id.issue_IMV);
        textView = (TextView) findViewById(R.id.backpacks_tv);
        textview1 = (TextView) findViewById(R.id.issue_tv);
        listView = (ListView) findViewById(R.id.list_item);


        gypsyModels = new ArrayList<>();
        gypsyModels.add(new GypsyModel(R.drawable.issue,"issue","Back packs",R.drawable.tickmark,R.drawable.noaction,"All Good","No issues"));
        gypsyModels.add(new GypsyModel(R.drawable.issue,"issue","Back Boards",R.drawable.tickmark,R.drawable.noaction,"All Good","No issues"));
        gypsyModels.add(new GypsyModel(R.drawable.issue,"issue","Boates",R.drawable.tickmark,R.drawable.noaction,"All Good","No issues"));
        gypsyModels.add(new GypsyModel(R.drawable.issue,"issue","Bicycles",R.drawable.tickmark,R.drawable.noaction,"All Good","No issues"));
        gypsyModels.add(new GypsyModel(R.drawable.issue,"issue","Basketball",R.drawable.tickmark,R.drawable.noaction,"All Good","No issues"));


        gypsyAdapter=new GypsyAdapter(gypsyModels,this);
        listView.setAdapter(gypsyAdapter);


    }

    @Override
    public void onClick(View view) {

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.callbutton_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.call_btn:
                Intent intent = new Intent(this, Callintro.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
