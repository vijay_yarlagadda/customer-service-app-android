package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.allmysons.customeserviceapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gajjala's on 11/1/2016.
 */

public class GypsyMothIndividual extends AppCompatActivity implements View.OnClickListener {
    TextView backpacks, backboards, bycles;
    ImageView good,noaction,issue;
    List<String> goodList;
    List<String> noactionList;
    List<String> issueList;
    int goodValue=0,noactionValue=0,issueesValue=0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gypsymoth_individual);

        goodList=new ArrayList<>();
        noactionList=new ArrayList<>();
        issueList=new ArrayList<>();

        noactionList.add("Boat Trailers");
        noactionList.add("Campers");
        noactionList.add("Ice Chests");

        issueList.add("Motorcycles");
        issueList.add("Motor Homes");
        issueList.add("Air Conditioners");

        goodList.add("Basketball \nBackboards");
        goodList.add("Bicycles");
        goodList.add("Boats");


        backpacks = (TextView) findViewById(R.id.backpacks_tv);
        backboards = (TextView) findViewById(R.id.backboards_tv);
        bycles = (TextView) findViewById(R.id.bycles_tv);

        good = (ImageView) findViewById(R.id.good_IMV);
        noaction = (ImageView) findViewById(R.id.noaction_IMV);
        issue = (ImageView) findViewById(R.id.issue_IMV);

        backpacks.setOnClickListener(this);
        backboards.setOnClickListener(this);
        bycles.setOnClickListener(this);

        good.setOnClickListener(this);
        noaction.setOnClickListener(this);
        issue.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.good_IMV) {

            if(goodValue<3) {
                backpacks.setText(goodList.get(goodValue));
            }
            goodValue++;
        }
        else if (view.getId() == R.id.noaction_IMV) {

            if(noactionValue<3) {
                backpacks.setText(noactionList.get(noactionValue));
            }
            noactionValue++;
        }
       else if (view.getId() == R.id.issue_IMV) {

            if(issueesValue<3) {
                backpacks.setText(issueList.get(issueesValue));
            }
            issueesValue++;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.callbutton_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.call_btn:
                Intent intent = new Intent(this, Callintro.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
