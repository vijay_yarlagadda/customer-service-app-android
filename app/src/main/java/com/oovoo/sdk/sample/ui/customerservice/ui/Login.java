package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telecom.Connection;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.allmysons.customeserviceapp.R;
import com.rethinkdb.RethinkDB;
import com.rethinkdb.RethinkDBConnection;

import static android.R.attr.host;
import static android.R.attr.port;
import static com.rethinkdb.RethinkDB.r;
import static java.lang.System.err;


/**
 * Created by Gajjala's on 9/27/2016.
 */

  public class Login extends AppCompatActivity {
    SharedPreferences userLogin;
    EditText code_et;

    public static final RethinkDB r = RethinkDB.r;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        final SharedPreferences userLogin;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

         RethinkDBConnection connection = new RethinkDBConnection("52.23.129.153",2015);
     /* com.rethinkdb.net.Connection conn = r.connection().hostname("127.0.0.1").port(29015).connect();*/
        r.db("vidivici").run(connection);
      /*  Cursor cursor = r.table("authors").run(connection);
        for (Object doc : cursor) {
            System.out.println(doc);
        }*/

        code_et = (EditText) findViewById(R.id.code_et);

        userLogin=getSharedPreferences("user", Context.MODE_PRIVATE);

        code_et.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() == 6) {
                    SharedPreferences.Editor editor=userLogin.edit();
                    editor.putString("user",code_et.getText().toString());
                    editor.commit();
                    Intent code_et = new Intent(Login.this,Callintro.class);
                    startActivity(code_et);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.callbutton_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.call_btn:
                Intent intent = new Intent(this, Callintro.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

