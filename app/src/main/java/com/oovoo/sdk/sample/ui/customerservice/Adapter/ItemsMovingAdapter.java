package com.oovoo.sdk.sample.ui.customerservice.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.allmysons.customeserviceapp.R;
import com.allmysons.customeserviceapp.model.Itemsmoving;
import com.allmysons.customeserviceapp.model.ScheduleApp;

import java.util.List;

/**
 * Created by Gajjala's on 9/19/2016.
 */

public class ItemsMovingAdapter extends BaseAdapter{
    List<Itemsmoving> itemsmovings;
    public  Context context;
    private static LayoutInflater inflater = null;

    public ItemsMovingAdapter(Context sContext, List<Itemsmoving> sitemsmovings) {
            itemsmovings=sitemsmovings;
            context=sContext;
        inflater = (LayoutInflater) sContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return itemsmovings.size();
    }

    @Override
    public Object getItem(int i) {
        return itemsmovings.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.items_moving, null);

        TextView time_tv = (TextView) vi.findViewById(R.id.bedroom_tv);
        ImageView  imageView = (ImageView) vi.findViewById(R.id.bed_img);
        ImageView  imageView2 = (ImageView) vi.findViewById(R.id.bed_more);
        ImageView  imageView3= (ImageView) vi.findViewById(R.id.bed_error);

        time_tv.setText(itemsmovings.get(i).data);
        imageView.setImageResource(itemsmovings.get(i).drawble);
        imageView2.setImageResource(itemsmovings.get(i).second);
        imageView3.setImageResource(itemsmovings.get(i).third);

        return vi;
    }
}
