package com.oovoo.sdk.sample.ui.customerservice.ui;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.allmysons.customeserviceapp.R;

/**
 * Created by Gajjala's on 10/12/2016.
 */

public class Home extends Fragment implements View.OnClickListener{

    LinearLayout confirmmovingneeds;
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        //Inflate the layout for this fragment
        View view = inflater.inflate(
                R.layout.homepage, container, false);

        confirmmovingneeds=(LinearLayout)view.findViewById(R.id.confirm_needs_LI);

        confirmmovingneeds.setOnClickListener(this);
        return view;
    }
    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.confirm_needs_LI){
            Intent home=new Intent(getActivity(),GypsyMoth.class);
            startActivity(home);
        }

    }
    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.callbutton_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.call_btn:
                // Do Fragment menu item stuff here

                Intent intent = new Intent(getActivity(), Callintro.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            default:
                break;
        }

        return false;
    }
}
