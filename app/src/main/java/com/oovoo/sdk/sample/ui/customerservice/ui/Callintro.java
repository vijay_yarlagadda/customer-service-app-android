package com.oovoo.sdk.sample.ui.customerservice.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.allmysons.customeserviceapp.R;

/**
 * Created by Gajjala's on 10/3/2016.
 */

public class Callintro extends AppCompatActivity implements View.OnClickListener {

    ImageView phone;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_intro);

        phone = (ImageView) findViewById(R.id.phone_green);
        phone.setTag("Green");
        phone.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.phone_green) {

            if(phone.getTag().equals("Green")) {

                phone.setImageResource(R.drawable.phone_red);
                phone.setTag("Red");

            }else{
                Intent callintro = new Intent(this, ConfirmInfo.class);
                startActivity(callintro);
                    phone.setImageResource(R.drawable.phone_green);
                    phone.setTag("Green");
            }
        }
    }
}
