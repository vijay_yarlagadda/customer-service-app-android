package com.oovoo.sdk.sample.ui.customerservice.model;

/**
 * Created by Gajjala's on 9/19/2016.
 */

public class Contactus
{
    public String imageurl;
    public String time;
    public String text;
    public boolean sender;

    public Contactus(String imageurl, String time, String text,boolean senders) {
        this.imageurl=imageurl;
        this.time=time;
        this.text=text;
        this.sender=senders;
    }
}