package com.oovoo.sdk.sample.ui.customerservice.model;

/**
 * Created by Gajjala's on 11/2/2016.
 */

public class GypsyModel {
    public int cross;
    public String backboards;
    public String issue;
    public int tickmark;
    public int noissue;
    public String goodtext;
    public String noissuetext;

    public GypsyModel(int cross, String backboards, String issue,int tickmark, int noissue, String goodtext, String noissuetext) {
        this.cross = cross;
        this.issue = issue;
        this.backboards = backboards;
        this.tickmark = tickmark;
        this.noissue = noissue;
        this.goodtext = goodtext;
        this.noissuetext = noissuetext;


    }
}
