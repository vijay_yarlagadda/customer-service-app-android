package com.oovoo.sdk.sample.ui.customerservice.model;

/**
 * Created by Gajjala's on 9/19/2016.
 */

public class ScheduleApp
{
    public String imageurl;
    public String time;
    public String date;


    public ScheduleApp(String imageurl, String time, String date) {
        this.imageurl=imageurl;
        this.time=time;
        this.date=date;

    }
}